# Overview
*Poly* is a multivariable polynomial calculator library written in *C*. It also comes with a parser and stack-based calculator for direct usage.  
To compile the parser, use  
```
md build
cd build
cmake ..
make
```  
You can also additionally call `make doc` to generate a doxygen document describing the library in detail.  
# Parser  
The parser supports the following commands:  
- `ZERO` - pushes a zero polynomial to the stack
- `IS_COEFF` - checks whether the polynomial at the top of the stack is a coefficient
- `IS_ZERO` - checks if the polynomial at the top of the stack is equal to 0
- `CLONE` - pushes a copy of the polynomial at the top of the stack to the stack
- `ADD` - pops two polynomials from the stack and pushes the result of their addition to it
- `MUL` - pops two polynomials from the stack and pushes the results of their multiplication to it
- `NEG` - negates the polynomial at the top of the stack
- `SUB` - pops two polynomials, subtracts the one below the top of the stack from the polynomial at the top and pushes the result to the stack
- `IS_EQ`- checks if the two topmost polynomials in the stack are equal
- `DEG` - returns the degree of the polynomial at the top of the stack
- `DEG_BY $id` - returns the degree of the polynomial at the top of the stack by variable `$id`
- `AT x` - calculate the value of the topmost polynomial at `x` and pushes it to the stack
- `PRINT` - prints the polynomial at the top of the stack in a parser-acceptable format
- `POP` - removes the polynomial at the top of the stack

The program can also parse polynomials represented by `(coeff,exp)` where `coeff` is a polynomial and `exp` is an unsigned integer.  
To express a sum, use the `+` symbol. Examples of parser-acceptable polynomials include:  
```
0
1
-2
(0,0)
(-2,0)
(1,0)+(1,2)
(1,2)+(1,0)
((1,2),15)+(-7,8)
(3,1)+(((4,4),100),2)
```