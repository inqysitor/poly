/** @file
   Implementacja klasy wielomianow.

   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-05-13
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "poly.h"

/**
 * \defgroup help_group Funkcje pomocnicze.
 * Statyczne funkcje niedostępne w interfejsie.
 * @{
 */

/**
 * Struktura pomocnicza pozwalająca usunąć warningi z PolyAddMonos.
 * (Konwertuje const Mono* na *Mono.)
 */
typedef union MonoPtr {
   Mono *ptr; ///< wskaźnik bez const
   const Mono* cptr; ///< wskaźnik do konwersji
} MonoPtr;


/**
 * Funkcja pomocnicza rekurencyjnie wypisująca wielomiany.
 * @param[in] p : wielomian
 * @param[in] depth : głębokość rekurencji
 */
static void PolyPrint_(const Poly* p, int depth);

/**
 * Funkcja pomocnicza sprawdzająca czy jednomiany w wielomianie są
 * posortowane rosnąco względem wykładników.
 * @param[in] p : wielomian
 * @return True jeśli posortowane, false w przeciwnym przypadku.
 */
static bool PolyIsSorted(Poly p);

/**
 * Funkcja pomocnicza sortująca tablicę jednomianów po wykładnikach(quicksort).
 * Sortuje elemnty tablicy w przedziale [left, right].
 * @param[in] monos : tablica jednomianów
 * @param[in] left : lewa granica sortowania
 * @param[in] right: prawa granica sortowania
 */
static void MonoSortByExp(Mono monos[], int left, int right);

/**
 * Funkcja pomocnicza obliczająca stopień jednomianu.
 * @param[in] m : jednomian.
 * @return stopień jednomianu
 */
static poly_exp_t MonoDeg(const Mono *m);

/**
 * Funkcja pomocnicza służąca do szybkiego potęgowania liczb całkowitych.
 * Algorytm: exponentiation by squaring
 * @param[in] base : podstawa
 * @param[in] n : wykładnik
 * @return wynik działania base^n
 */
static poly_coeff_t FastPow(poly_coeff_t base, poly_exp_t n);

/**
 * Funkcja pomocnicza zamieniająca wielomian stały na współczynnik.
 * @param[in] p : wielomian
 * @return wielomian będący współczynnikiem
 */
static Poly PolyToCoeff(const Poly *p);

/**
 * Funkcja pomocnicza dodająca wielomian stały do wielomianu o stopniu >0.
 * @param[in] coeff : wielomian stały
 * @param[in] poly : wielomian o stopniu > 0
 * @returns Suma wielomianów.
 */
static Poly PolyAddCoeffToPoly(const Poly *coeff, const Poly *poly);

/**
 * Funkcja pomocnicza mnożąca wielomian stały przez wielomian o stopniu >0.
 * @param[in] coeff : wielomian stały
 * @param[in] poly : wielomian o stopniu > 0
 * @returns Iloczyn wielomianów.
 */
static Poly PolyMulCoeffAndPoly(const Poly* coeff, const Poly* poly);

/**
 * Funkcja pomocnicza potęgująca wielomian
 * @param[in] poly : wielomian będący podstawą
 * @param[in] n : wykładnik do którego ma zostać podniesiony wielomian
 */
static Poly PolyPow(const Poly* poly, poly_exp_t n);

/**
 * Funkcja rekurencyjna pomocnicza do PolyCompose 
 * Wykonuje MonoCompose_ dla każdego z jednomianów składowych, a następnie je sumuje
 * @param[in] p : wielomian bazowy
 * @param[in] count : liczba wielomianów w tablicy x
 * @param[in] x : tablica wielomianów do podstawiania
 * @param[in] index : głębokość rekurencji
 */
static Poly PolyCompose_(const Poly *p, unsigned count, const Poly x[], unsigned index);

/**
 * Funkcja rekurencyjna pomocnicza do PolyCompose
 * Wykonuje PolyCompose_ dla współczynnika jednomianu i podkłada wielomian oraz podnosi go do potęgi.
 * @param[in] m : jednomian bazowy
 * @param[in] count : liczba wielomianów w tablicy x
 * @param[in] x : tablica wielomianów do podstawiania
 * @param[in] index : głębokość rekurencji
 */
static Poly MonoCompose_(const Mono *m, unsigned count, const Poly x[], unsigned index);

/**
 * @}
 */

void PolyDestroy(Poly *p)
{
	if (!PolyIsCoeff(p)) {
		for (int i = 0; i < p->size; i++) {
			MonoDestroy(&(p->monos[i]));
		}

		free(p->monos);
		p->size = 0;
		p->coeff = -1;
	}

	p->monos = NULL;
	p->size = 0;
	p->coeff = -1;
}

void MonoDestroy(Mono *m)
{
  PolyDestroy(&(m->p));
}

Poly PolyClone(const Poly *p)
{
	if (PolyIsCoeff(p)) {		
		return (Poly) { .monos = NULL, .size = 0, .coeff = p->coeff};
	}

	Mono *monos = malloc(p->size * sizeof(Mono));
	assert(monos != NULL);
	for (int i = 0; i < p->size; i++) {
		monos[i] = MonoClone(&(p->monos[i]));
	}

	return (Poly) { .monos = monos, .size = p->size, .coeff = p->coeff};
}

Poly PolyAdd(const Poly *p, const Poly *q)
{
	if (PolyIsZero(p)) {
		return PolyClone(q);
	}

	if (PolyIsZero(q)) {
		return PolyClone(p);
	}

	if (!PolyIsCoeff(p) && !PolyIsCoeff(q)) {
		int out_size = p->size + q->size;
		Mono* out_monos = malloc(out_size*sizeof(Mono));
		assert(out_monos != NULL);

		Mono* p_monos = p->monos;
		Mono* q_monos = q->monos;

		if (!PolyIsSorted(*p)) {
			MonoSortByExp(p_monos, 0, p->size-1);
		}
		if (!PolyIsSorted(*q)) {
			MonoSortByExp(q_monos, 0, q->size-1);
		}
		

		int i, j, k;
		i = j = k = 0;

		bool running = true;

		while (running) {
			while (i < p->size && ( j >= q->size || p_monos[i].exp < q_monos[j].exp)) {
				out_monos[k] = MonoClone(&p_monos[i]);
				i++;
				k++;
			}

			while (i < p->size && j < q->size && p_monos[i].exp == q_monos[j].exp) {
				Poly tmp = PolyAdd(&(p_monos[i].p),&(q_monos[j].p));
				if (!PolyIsZero(&tmp)) {
					out_monos[k] = MonoFromPoly(&tmp, p_monos[i].exp);
					i++;
					j++;
					k++;
				} else {
					PolyDestroy(&tmp);
					i++;
					j++;
				}
			}

			while (j < q->size && ( i>= p->size || q_monos[j].exp < p_monos[i].exp)) {
				out_monos[k] = MonoClone(&q_monos[j]);
				j++;
				k++;
			}

			if(i == p->size && j == q->size) {
				running = false;
			}
		}

		Poly result = PolyAddMonos(k, out_monos);
		free(out_monos);
		return result;
	} 
	else if (PolyIsCoeff(p) && PolyIsCoeff(q)) {
		return PolyFromCoeff(p->coeff + q->coeff);
	} 
	else if (PolyIsCoeff(p) && !PolyIsCoeff(q)) {
		return PolyAddCoeffToPoly(p, q);
	/*	Mono m = MonoFromPoly(p, 0);

		Mono* monos = malloc(sizeof(Mono));
		assert(monos != NULL);
		monos[0] = m;

		Poly p2 = (Poly) { .monos = monos, .size = 1, .coeff = 0 };;
		Poly result = PolyAdd(&p2,q);
		MonoDestroy(&m);
		PolyDestroy(&p2);
		return result; */
	} 
	else /*if(!PolyIsCoeff(p) && PolyIsCoeff(q))*/ {
		return PolyAddCoeffToPoly(q, p);
		/*Mono m = MonoFromPoly(q, 0);

		Mono* monos = malloc(sizeof(Mono));
		assert(monos != NULL);
		monos[0] = m;

		Poly q2 = (Poly) { .monos = monos, .size = 1, .coeff = 0 };;
		Poly result = PolyAdd(p, &q2);
		MonoDestroy(&m);
		PolyDestroy(&q2);
		return result;*/
	}
	
}

static Poly PolyToCoeff(const Poly *p)
{
	Poly temp = PolyClone(p);
	while(!PolyIsCoeff(&temp)) {
		Poly temp2 = PolyAt(&temp, 1);
		PolyDestroy(&temp);
		temp = temp2;
	}

	return temp;
}

Poly PolyAddMonos(unsigned count, const Mono monos[])
{
	if(count == 0) {
		return PolyZero();
	}

	Mono* filtered_monos = malloc(count * sizeof(Mono));
	assert(filtered_monos != NULL);

	int shift = 0;
	bool found_hidden_coeff = false;
	Poly hidden_coeff = PolyZero();

	for (size_t i = 0; i < count; i++) {
		if (!PolyIsZero(&(monos[i].p))) {
			if (monos[i].exp == 0 && PolyIsCoeff(&(monos[i].p)))/*PolyDeg(&(monos[i].p)) == 0)*/ {
				Poly new_coeff = PolyToCoeff(&(monos[i].p));

				if (!PolyIsZero(&new_coeff)) {
					Poly new_hidden_coeff = PolyAdd(&hidden_coeff, &new_coeff);
					PolyDestroy(&hidden_coeff);
					hidden_coeff = new_hidden_coeff;
					found_hidden_coeff = true;
				}
				
				PolyDestroy(&new_coeff);

				MonoPtr destroy_helper = (MonoPtr) { .cptr = &monos[i] };
				MonoDestroy(destroy_helper.ptr);
				shift--;
			} 
			else {
				filtered_monos[i+shift] = monos[i];
			}
			
		} 
		else {
			MonoPtr destroy_helper = (MonoPtr) { .cptr = &monos[i] };
			MonoDestroy(destroy_helper.ptr);
			shift--;
		}
		
	}
	count = count + shift;

	if(count == 0) {
		free(filtered_monos);
		return hidden_coeff;
	}

	Mono* output_monos = malloc((count+1) * sizeof(Mono));
	assert(output_monos != NULL);

	int om_index = 0;
	MonoSortByExp(filtered_monos, 0, (int)count-1);
	for (size_t i = 0; i < count-1; i++) {
		if (filtered_monos[i].exp == filtered_monos[i+1].exp) {
			Poly monoMaterial = PolyAdd(&(filtered_monos[i].p),&(filtered_monos[i+1].p));
			if (!PolyIsZero(&monoMaterial)) {
				Mono newMono = MonoFromPoly(&monoMaterial,filtered_monos[i].exp);
				MonoDestroy(&(filtered_monos[i]));
				MonoDestroy(&(filtered_monos[i+1]));
				filtered_monos[i].exp = -1;
				filtered_monos[i+1] = newMono;
			} 
			else {
				MonoDestroy(&(filtered_monos[i]));
				MonoDestroy(&(filtered_monos[i+1]));
				filtered_monos[i].exp = -1;
				filtered_monos[i+1].exp = -1;
				PolyDestroy(&monoMaterial);
			}
		}
	}

	if (found_hidden_coeff) {
		Mono head = MonoFromPoly(&hidden_coeff, 0);
		output_monos[om_index++] = head;
	} 
	else {
		PolyDestroy(&hidden_coeff);
	}

	for (unsigned int i = 0; i < count; i++) {
		if (filtered_monos[i].exp != -1) {
			output_monos[om_index++] = filtered_monos[i];
		}
	}

/*	if (found_hidden_coeff) {
		Mono tail = MonoFromPoly(&hidden_coeff, 0);
		output_monos[om_index++] = tail;
	} 
	else {
		PolyDestroy(&hidden_coeff);
	} */

	free(filtered_monos);

	Poly p;
	if (om_index > 0) {
		p = (Poly) {.monos = output_monos, .size = om_index, .coeff = 0};
	} 
	else {
		free(output_monos);
		p = PolyZero();
	}

	return p;
}

Poly PolyMul(const Poly *p, const Poly *q)
{
	Mono* output_monos;
	int k = 0;
	if (!PolyIsCoeff(p) && !PolyIsCoeff(q)) {
		output_monos = malloc(p->size*q->size*sizeof(Mono));
		assert(output_monos != NULL);
		for (int i = 0; i < p->size; i++) {
			for (int j = 0; j < q->size; j++) {
				Poly tmp = PolyMul(&(p->monos[i].p),&(q->monos[j].p));
				if(!PolyIsZero(&tmp)) {
					output_monos[k] = (Mono) { .p = tmp, .exp = p->monos[i].exp + q->monos[j].exp};
					k++;
				}	
			}
		}
	} else if(PolyIsCoeff(p) && !PolyIsCoeff(q)) {
		return PolyMulCoeffAndPoly(p, q);
	} else if(!PolyIsCoeff(p) && PolyIsCoeff(q)) {
		return PolyMulCoeffAndPoly(q, p);
	} else { // if(PolyIsCoeff(p) && PolyIsCoeff(q))
		return PolyFromCoeff(p->coeff * q->coeff);
	}

	Poly result = PolyAddMonos(k, output_monos);;
	free(output_monos);

	return result;
}

Poly PolyNeg(const Poly *p)
{
	if (PolyIsCoeff(p)) {
		return PolyFromCoeff(p->coeff*-1);
	}
	else {
		Mono* monos = p->monos;

		Mono* output_monos = malloc(p->size * sizeof(Mono));
		assert(output_monos != NULL);
		Poly minus_one = PolyFromCoeff(-1);
		for (int i = 0; i < p->size; i++) {
			Poly mul = PolyMul(&(monos[i].p), &minus_one);
			output_monos[i] = MonoFromPoly(&mul, monos[i].exp);
		}
		PolyDestroy(&minus_one);

		Poly result = PolyAddMonos(p->size, output_monos);
		free(output_monos);
		return result;
	}
}

Poly PolySub(const Poly *p, const Poly *q)
{
	Poly tmp = PolyNeg(q);
	Poly result = PolyAdd(p, &(tmp));
	PolyDestroy(&tmp);
	return result;
}

poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx)
{
	if(PolyIsZero(p)) {
		return -1;
	}

	if(PolyIsCoeff(p)) {
		return 0;
	}

	Mono* monos = p->monos;
	if(var_idx == 0) {
		int exp_max = -1;
		for (int i = 0; i < p->size; i++) {
			if (monos[i].exp > exp_max) {
				exp_max = monos[i].exp;
			}
		}

		return exp_max;
	}
	else {
		int exp_max = -1;
		for (int i = 0; i < p->size; i++) {
			poly_exp_t temp = PolyDegBy(&(monos[i].p), var_idx-1);
			if (temp > exp_max) {
				exp_max = temp;
			}
		}

		return exp_max;
	}

}

static poly_exp_t MonoDeg(const Mono *m)
{
	int max_exp = 0;
	for (int i = 0; i < m->p.size; i++) {
		poly_exp_t exp = MonoDeg(&(m->p.monos[i]));
		if (exp > max_exp) {
			max_exp = exp;
		}
	}

	return m->exp + max_exp;
}

poly_exp_t PolyDeg(const Poly *p)
{
	if(PolyIsZero(p)) {
		return -1;
	}

	if(PolyIsCoeff(p)) {
		return 0;
	}

	int max_exp = 0;
	for (int i = 0; i < p->size; i++) {
		poly_exp_t exp = MonoDeg(&(p->monos[i]));
		if (exp > max_exp) {
			max_exp = exp;
		}

	}

	return max_exp;
}

bool PolyIsEq(const Poly *p, const Poly *q)
{
	if (p->size != q->size) {
		return false;
	}

	if (p->size == 0 && q->size == 0) {
		return p->coeff == q->coeff;
	}

	if (!PolyIsSorted(*p)) {
		MonoSortByExp(p->monos, 0, p->size-1);
	}

	if (!PolyIsSorted(*q)) {
		MonoSortByExp(q->monos, 0, q->size-1);
	}

	for (int i = 0;i < p->size;i++) {
		Mono mp = p->monos[i];
		Mono mq = q->monos[i];
		bool flag = (mp.exp == mq.exp) && PolyIsEq(&(mp.p), &(mq.p));
		if (!flag) {
			return flag;
		}
	}
	return true;
}

static poly_coeff_t FastPow(poly_coeff_t base, poly_exp_t n)
{
	if (n == 0) {
		return 1;
	}

	if (n % 2 == 1) {
		poly_coeff_t r = FastPow(base, (n-1)/2);
		return base * r * r;
	}

	poly_coeff_t r = FastPow(base, n/2);
	return r*r;
}

Poly PolyAt(const Poly *p, poly_coeff_t x)
{
	if(PolyIsCoeff(p)) {
		return PolyClone(p);
	}

	Poly output = PolyZero();
	for (int i = 0; i < p->size; i++) {
		Mono m = p->monos[i];
		Poly multiplier = PolyFromCoeff(FastPow(x, m.exp));
		Poly addition = PolyMul(&(m.p), &multiplier);
		PolyDestroy(&multiplier);
		Poly tmp = PolyAdd(&output, &addition);
		PolyDestroy(&addition);
		PolyDestroy(&output);
		output = tmp;
	}
	return output;
}

static Poly PolyCompose_(const Poly *p, unsigned count, const Poly x[], unsigned index);

static Poly MonoCompose_(const Mono *m, unsigned count, const Poly x[], unsigned index) {
	Poly base;
	if(index < count) {
		base = x[index];
	} else {
		base = PolyZero();
	}

	Poly newBase = PolyPow(&base, m->exp);
	Poly newCoeff = PolyCompose_(&m->p, count, x, index+1);
	Poly result = PolyMul(&newBase,&newCoeff);
	PolyDestroy(&newBase);
	PolyDestroy(&newCoeff);
	return result;
}

static Poly PolyCompose_(const Poly *p, unsigned count, const Poly x[], unsigned index) {
	if(!PolyIsCoeff(p)) {
		Poly result = PolyZero();
		for(int i = 0; i < p->size; i++) {
			Poly compose = MonoCompose_(&(p->monos[i]), count, x, index);
			Poly tmp = PolyAdd(&result, &compose);
			PolyDestroy(&compose);
			PolyDestroy(&result);
			result = tmp;
		}
		return result;
	} else {
		return PolyClone(p);
	}
	
	
}

Poly PolyCompose(const Poly *p, unsigned count, const Poly x[]) {
	if(count == 0) {
		return PolyAt(p, 0);
	}
	return PolyCompose_(p, count, x, 0);
}

void MonoPrint(const Mono *m)
{
	printf("(");
	PolyPrint_(&(m->p), 1);
	printf(",%d)", m->exp);
}

void PolyPrint(const Poly *p)
{
	PolyPrint_(p, 0);
	printf("\n");
}

static void PolyPrint_(const Poly *p, int depth)
{
	if (PolyIsCoeff(p)) {
		printf("%ld", p->coeff);
	}
	else {
		for (int i = 0; i < p->size; i++) {
			Mono m = p->monos[i];
			printf("(");
			PolyPrint_(&(m.p), depth+1);
			printf(",%d)", m.exp);

			if(i<p->size-1) {
				printf("+");
			}
		}
	}
}

static bool PolyIsSorted(Poly p)
{
	for(int i = 0; i< p.size-1; i++) {
		if(p.monos[i].exp > p.monos[i+1].exp) {
			return false;
		}
	}

	return true;
}

static void MonoSortByExp(Mono monos[], int left, int right) {
	int i = left, j = right;
	int pivot = monos[(left + right) / 2].exp;
	Mono tmp;

	while (i <= j)  {
		while (monos[i].exp < pivot)  {
			i++;
		}
		while (monos[j].exp > pivot) {
			j--;
		}
			
		if (i <= j) {
			tmp = monos[i];
			monos[i] = monos[j];
			monos[j] = tmp;
			i++;
			j--;
		}
	}

	if (left < j) {
		MonoSortByExp(monos, left, j);
	}

	if (i < right) {
		MonoSortByExp(monos, i, right);
	}
}

static Poly PolyAddCoeffToPoly(const Poly *coeff, const Poly *poly) {
		Mono m = MonoFromPoly(coeff, 0);

		Mono* monos = malloc(sizeof(Mono));
		assert(monos != NULL);
		monos[0] = m;

		Poly coeff2 = (Poly) { .monos = monos, .size = 1, .coeff = 0 };;
		Poly result = PolyAdd(&coeff2,poly);
		MonoDestroy(&m);
		PolyDestroy(&coeff2);
		return result;
}

static Poly PolyMulCoeffAndPoly(const Poly* coeff, const Poly* poly){
	int k = 0;
	Mono* output_monos = malloc(poly->size*sizeof(Mono));
	assert(output_monos != NULL);
	for (int j = 0; j < poly->size; j++) {
		Poly tmp = PolyMul(coeff,&(poly->monos[j].p));
		if(!PolyIsZero(&tmp)) {
			output_monos[k] = (Mono) { .p = tmp, .exp = poly->monos[j].exp};
			k++;
		}
	}

	Poly result = PolyAddMonos(k, output_monos);
	free(output_monos);

	return result;
}

static Poly PolyPow(const Poly* poly, poly_exp_t n) {
	if (n == 0) {
		return PolyFromCoeff(1);
	}

	if(!PolyIsCoeff(poly)) {
		if (n % 2 == 1) {
			Poly r = PolyPow(poly, (n-1)/2);
			Poly result1 = PolyMul(&r,&r);
			Poly output = PolyMul(poly, &result1);
			PolyDestroy(&r);
			PolyDestroy(&result1);
			return output;
		}

		Poly r = PolyPow(poly, n/2);
		Poly output = PolyMul(&r,&r);
		PolyDestroy(&r);
		return output;
	} else {
		return PolyFromCoeff(FastPow(poly->coeff, n));
	}
	
}