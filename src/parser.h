/** @file
   Interfejs parsera wielomianów i operacji na wielomianach.

   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-05-24
*/

#ifndef __PARSER_H__
#define __PARSER_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "poly.h"

/** Max command length */
#define COMMAND_MAX_LENGTH 12

/** 
 * @name Napisy komend
 * @brief Napisy identyfikujące komendy.
 */
///@{
#define COMMAND_ZERO "ZERO"
#define COMMAND_IS_COEFF "IS_COEFF"
#define COMMAND_IS_ZERO "IS_ZERO"
#define COMMAND_CLONE "CLONE"
#define COMMAND_ADD "ADD"
#define COMMAND_MUL "MUL"
#define COMMAND_NEG "NEG"
#define COMMAND_SUB "SUB"
#define COMMAND_IS_EQ "IS_EQ"
#define COMMAND_DEG "DEG"
#define COMMAND_DEG_BY "DEG_BY"
#define COMMAND_AT "AT"
#define COMMAND_PRINT "PRINT"
#define COMMAND_POP "POP" 
#define COMMAND_PUSHONE "PUSHONE"
#define COMMAND_COMPOSE "COMPOSE"
///@}


/** 
 * @name Identyfikatory komend
 * @brief Identfikatory liczbowe dla komend.
 */
///@{
//* Oznacza, że nie istnieje komenda zadana napisem. */
#define ID_COMMAND_NOT_FOUND -1
#define ID_ZERO 0
#define ID_IS_COEFF 1
#define ID_IS_ZERO 2
#define ID_CLONE 3
#define ID_ADD 4
#define ID_MUL 5
#define ID_NEG 6
#define ID_SUB 7
#define ID_IS_EQ 8
#define ID_DEG 9
#define ID_DEG_BY 10
#define ID_AT 11
#define ID_PRINT 12
#define ID_POP 13 
#define ID_PUSHONE 14
#define ID_COMPOSE 15
///@}

/** 
 * @name Identyfikatory błędów
 * @brief Identfikatory liczbowe dla błędów.
 */
///@{
#define ERROR_NOT_AN_ERROR -1
#define ERROR_COMMAND 1
#define ERROR_VARIABLE 2
#define ERROR_VALUE 3
#define ERROR_POLY 4
#define ERROR_UNDERFLOW 5
#define ERROR_COUNT 6
///@}

/**
 * Typ zmiennej całkowitoliczbowej używanej przez funkcje wczytujące liczby.
 */
typedef long parser_arithmetic_t;

/**
 * Format danych zwracanych przez LoadPoly.
 */
typedef struct LoadPolyResult {
	bool success;///<True gdy wielomian został pomyślnie wczytany, false wpp.
	int column;///<Jeżeli wystąpił błąd wczytywania, oznacza pierwszą niepoprawną kolumnę.
	Poly p;///<Utworzony wielomian, jeśli wczytywanie się powiodło.
} LoadPolyResult;

/**
 * Format danych zwracanych przez LoadCommand.
 */
typedef struct LoadCommandResult{
	bool success;///<True gdy wielomian został pomyślnie wczytany, false wpp.
	int errorId;///<Identyfikator błędu, eżeli wystąpił błąd wczytywania.
	int commandId;///<Id wczytanej komendy jeśli wczytywanie się powiodło.
	parser_arithmetic_t param;///<Utworzony parametr, jeśli wczytywanie się powiodło i komenda go wymaga.
} LoadCommandResult;

/**
 * Sprawdza, czy dany znak jest literą
 * @param[in] c : char
 * @returns True, jeśli znak c jest literą, false w przeciwnym przypadku.
 */
static inline bool IsLetter(char c)
{
	return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

/**
 * Szuka komendy odpowiedającej ciągowi znaków.
 * @param[in] command : Tablica znaków.
 * @returns Jeżeli komenda została znaleziona, to jej ID, ID_COMMAND_NOT_FOUND wpp.
 */
int ParseCommand(const char* command);

/**
 * Wczytuje znaki do następnego znaku końca linii lub końca pliku włącznie.
 * @returns bool, True jeżeli standardowe wejście nie zawierało innych znaków, nie będących
 * końcem linii lub końcem pliku.
 */
bool EatLine();

/**
 * Funkcja wczytująca wielomian z wejścia.
 * @param[in] row : numer wiersza z którego wczytywany był wielomian
 * @param[in] column : obecnie wczytywana kolumna
 * @param[in] depth : głębokość rekurencji, wywołując funkcję z zewnątrz należy użyć 0.
 * @return struktura LoadPolyResult zawierająca informacje o powodzeniu wczytywania, wczytany
 * wielomian w wypadku sukcesu i numer kolumny w której wystąpił błąd w razie niepowodzenia.
 */
LoadPolyResult LoadPoly(int row, int* column, int depth);

/**
 * Funkcja wczytująca komendę z wejścia.
 * @param[in] row : numer wiersza z którego wczytywana jest komenda,
 * @return struktura LoadCommandResult zawierająca informacje o powodzeniu wczytywania,
 * id wczytanej komendy i jej parametr lub id błędu w przypadku niepowodzenia.
 */
LoadCommandResult LoadCommand(int* row);

/**
 * Funkcja wypisująca komunikat o błędzie na standardowy strumień błędów.
 * @param[in] errorId : Id błędu.
 * @param[in] row : numer wiersza w którym wystąpił błąd
 * @param[in] column : numer kolumny w której wystąpił błąd, jeśli potrzebna(dowolna wpp).
 */
void PrintError(int errorId, int row, int column);


#endif /* __PARSER_H__ */
	