/** @file
   Interfejs stosu wielomianów.

   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @date 2017-05-22
*/

#ifndef __STACK_H__
#define __STACK_H__

#include <stdbool.h>

#include "poly.h"

/**
 * \defgroup stack_group Interfejs stosu.
 * Główne funkcje i struktury służące do obsługi stosu wielomianów.
 * @{
 */

struct PolyStackElement;

/**
 * Struktura stosu wielomianów.
 * Implementuje standardowe operacje na stosie w czasie stałym.
 */
typedef struct PolyStack {
	struct PolyStackElement* head; ///<Wielomian na wierzchołku stosu.
	unsigned size; ///<Rozmiar stosu.
} PolyStack;

/**
 * Struktura elementów stosu. (=Lista jednokierunkowa.)
 */
typedef struct PolyStackElement {
	struct PolyStackElement* next; ///<Następny element listy.
	Poly p; ///<Przechowywany wielomian.
} PolyStackElement;

/**
 * Tworzy nowy, pusty stos.
 * @return stos
 */
static inline PolyStack StackNew() {
	return (PolyStack) { .head = NULL, .size = 0 };
}

/**
 * Sprawdza, czy dany stos jest pusty.
 * @param[in] s : stos
 * @return bool
 */
static inline bool StackIsEmpty(PolyStack* s) {
	return s->size == 0;
}

/**
 * Sprawdza, czy dany stos jest przynajmniej wielkości danej na parametr.
 * @param[in] s : stos
 * @param[in] size : minimalna wielkość
 * @return bool
 */
static inline bool StackVerifySize(PolyStack* s, unsigned size) {
	return s->size >= size;
}

/**
 * Wkłada wielomian na wierzchołek stosu.
 * @param[in] s : stos
 * @param[in] p : wielomian
 * @return bool
 */
void StackPush(PolyStack* s, Poly p);

/**
 * Ściąga i zwraca wielomian z wierzchołka stosu. 
 * Operacja jest błędna jeżeli na stosie nie ma wielomianów.
 * StackIsEmpty pozwala zweryfikować istnienie wielomianu na stosie.
 * @param[in] s : stos
 * @return wielomian
 */
Poly StackPop(PolyStack* s);

/**
 * Pozwala podejrzeć wielomian nie ściągając go z wierzchołka stosu.
 * Operacja jest błędna jeżeli na stosie nie ma wielomianów.
 * StackIsEmpty pozwala zweryfikować istnienie wielomianu na stosie.
 * @param[in] s : stos
 * @return wielomian
 */
Poly StackPeek(PolyStack* s);

/**
 * Niszczy stos. 
 * Wywołuje PolyDestroy dla każdego wielomianu na stosie.
 * @param[in] s : stos
 */
void StackDestroy(PolyStack* s);

/**
 * @}
 */

#endif /* __STACK_H__ */