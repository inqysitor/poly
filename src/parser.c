/** @file
   Implementacja parsera wielomianów i operacji na wielomianach. Wczytuje wielomiany i komendy z wejścia.

   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @date 2017-05-24
*/
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "parser.h"

/**
 * Sprawdza, czy dany znak jest cyfrą.
 * @param[in] c : char
 * @returns True, jeśli znak c jest cyfrą, false w przeciwnym przypadku.
 */
static inline bool IsDigit(char c);

/**
 * Funkcja pomocnicza służąca do szybkiego potęgowania liczb całkowitych.
 * Algorytm: exponentiation by squaring
 * @param[in] base : podstawa
 * @param[in] n : wykładnik
 * @return wynik działania base^n
 */
static parser_arithmetic_t FastPow(parser_arithmetic_t base, int n);

/**
 * Wczytuje komendę ze standardowego wyjścia. Poprawna komenda składa się z liter i znaku '_'.
 * Maksymalna długość komendy jest zdefiniowana przez COMMAND_MAX_LENGTH.
 * @param[in] command : wskaźnik na tablicę liter rozmiaru conajmniej COMMAND_MAX_LENGTH
 * @return true jeżeli wczytywanie się powiodło, false w przeciwnym wypadku.
 */
static bool LoadWord(char* command);

/**
 * Sprawdza, czy wartość bezwzględna liczby reprezentowana przez tablicę liter jest większa,
 * niż wartość bezwzględna przyjętej granicy.
 * @param[in] input : tablica liter
 * @param[in] input_length: długość tablicy liter
 * @param[in] number : granica
 * @return bool
 */
static bool IsInputBiggerThanBound(char* input, int input_length, parser_arithmetic_t number);

/**
 * Wczytuje liczbę ze standardowego wejścia.
 * Przestaje wczytywać liczbę, gdy ta przekroczy zakres lub gdy natrafi na znak niebędący cyfrą.
 * @param[in] low_bound: dolna granica wartości liczby, musi być mniejsza lub równa 0
 * @param[in] high_bound: górna granica wartości liczby, musi być większa lub równa 0
 * @param[in] output: wskaźnik na liczbę, na którą wpisany ma zostać wynik wczytywania.
 * @param[in] column: wskaźnik na numer obecnej kolumny, funkcja zwiększy go o liczbę
 * kolumn, które wczytała.
 * @return -1, jeżeli wczytywanie zostało zakończone sukcesem lub numer kolumny na której 
 * funkcja natrafiła na błąd danych(licząc od 0).
 */
static int LoadNumber(parser_arithmetic_t low_bound, parser_arithmetic_t high_bound, parser_arithmetic_t* output, int* column);

/**
 * Funkcja pomocnicza zwalaniająca pamięć zaalokowaną przez LoadPoly i wypisująca błąd.
 * @param[in] monos : tablica jednomianów
 * @param[in] index : ilość utworzonych przez LoadPoly wielomianów
 */
static void LoadPolyCleanup(Mono* monos, int index);

/**
 * Funkcja pomocnicza tworząca strukturę typu LoadCommandResult.
 * @param[in] s : powodzenie wczytywania
 * @param[in] e : identyfikator błędu
 * @param[in] c : identyfikator komendy
 * @param[in] p : parametr komendy
 */
static LoadCommandResult NewLoadCommandResult(bool s, int e, int c, parser_arithmetic_t p);

/**
 * Funkcja pomocnicza tworząca strukturę typu LoadPolyResult.
 * @param[in] s : powodzenie wczytywania
 * @param[in] p : wczytany wielomian
 * @param[in] c : kolumna w której wystąpił błąd
 */
static LoadPolyResult NewLoadPolyResult(bool s, Poly p, int c);


int ParseCommand(const char* command)
{
	if(strcmp(command, COMMAND_ZERO) == 0) {
		return ID_ZERO;
	} else if(strcmp(command, COMMAND_IS_COEFF) == 0) {
		return ID_IS_COEFF;
	} else if(strcmp(command, COMMAND_IS_ZERO) == 0) {
		return ID_IS_ZERO;
	} else if(strcmp(command, COMMAND_CLONE) == 0) {
		return ID_CLONE;
	} else if(strcmp(command, COMMAND_ADD) == 0) {
		return ID_ADD;
	} else if(strcmp(command, COMMAND_MUL) == 0) {
		return ID_MUL;
	} else if(strcmp(command, COMMAND_NEG) == 0) {
		return ID_NEG;
	} else if(strcmp(command, COMMAND_SUB) == 0) {
		return ID_SUB;
	} else if(strcmp(command, COMMAND_IS_EQ) == 0) {
		return ID_IS_EQ;
	} else if(strcmp(command, COMMAND_DEG) == 0) {
		return ID_DEG;
	} else if(strcmp(command, COMMAND_DEG_BY) == 0) {
		return ID_DEG_BY;
	} else if(strcmp(command, COMMAND_AT) == 0) {
		return ID_AT;
	} else if(strcmp(command, COMMAND_PRINT) == 0) {
		return ID_PRINT;
	} else if(strcmp(command, COMMAND_POP) == 0) {
		return ID_POP;
	} else if(strcmp(command, COMMAND_PUSHONE) == 0) {
		return ID_PUSHONE;
	} else if(strcmp(command, COMMAND_COMPOSE) == 0) {
		return ID_COMPOSE;
	} else {
		return ID_COMMAND_NOT_FOUND;
	}
}

bool EatLine()
{
	bool running = true;
	char c;
	bool wasEmpty = true;
	while(running && (c = getchar())) {
		if(c == '\n' || c == '\0' || c == EOF) {
			running = false;
		} else {
			wasEmpty = false;
		}
	}

	return wasEmpty;
}

LoadPolyResult LoadPoly(int row, int* column, int depth)
{
	bool running = true;
	char c;
	int parColumn = *column;
	while(running && (c = getchar())) {
		(*column)++;
		if(IsDigit(c) || c == '-'){
			parser_arithmetic_t coeff = 0;
			ungetc(c,stdin);
			(*column)--;
			int oldColumn = *column;
			int success = LoadNumber(LONG_MIN, LONG_MAX, &coeff, column);
			if(success == -1) {
				return NewLoadPolyResult(true, PolyFromCoeff((poly_coeff_t)coeff), *column+1);
				//return (LoadPolyResult){.success = true, .p = PolyFromCoeff((poly_coeff_t)coeff), .column = *column+1};
			} else {
				return NewLoadPolyResult(false, PolyZero(), oldColumn+success);
				//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = oldColumn+success};
			}
		} else if(c == '(') {
			bool running = true;
			int monos_size = 4;
			Mono* monos = malloc(sizeof(Mono) * monos_size);
			int index = 0;

			while(running) {
				parser_arithmetic_t exp = 0;
				LoadPolyResult coeff_t = LoadPoly(row, column, depth+1);
				if(!coeff_t.success) {
					LoadPolyCleanup(monos,index);
					return NewLoadPolyResult(false, PolyZero(), coeff_t.column);
					//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = coeff_t.column};
				}

				Poly coeff = coeff_t.p;

				c = getchar();
				(*column)++;
				if(c != ',') {
					ungetc(c, stdin);
					if(coeff_t.success){ PolyDestroy(&coeff);}
					LoadPolyCleanup(monos,index);
					parColumn = *column;
					(*column)--;
					return NewLoadPolyResult(false, PolyZero(), parColumn);
					//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = parColumn};
				}

				int oldColumn = *column;
				int success = LoadNumber(0, UINT_MAX, &exp, column);
				if(success != -1) {
					if(coeff_t.success){ PolyDestroy(&coeff);}
					LoadPolyCleanup(monos,index);
					return NewLoadPolyResult(false, PolyZero(), success+oldColumn);
					//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = success+oldColumn};
				}

				c = getchar();
				(*column)++;
				if(c != ')') {
					ungetc(c, stdin);
					if(coeff_t.success){ PolyDestroy(&coeff);}
					LoadPolyCleanup(monos,index);
					parColumn = *column;
					(*column)--;
					return NewLoadPolyResult(false, PolyZero(), parColumn);
					//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = parColumn};
				}

				if(index >= monos_size){
					monos_size = monos_size*2;
					monos = realloc(monos, monos_size*sizeof(Mono));
				}
				monos[index++] = MonoFromPoly(&coeff, (poly_exp_t)exp);

				c = getchar();
				(*column)++;
				if(c == '+'){
					c = getchar();
					(*column)++;
					if(c != '(') {
						ungetc(c, stdin);
						LoadPolyCleanup(monos, index);
						parColumn = *column;
						(*column)--;
						return NewLoadPolyResult(false, PolyZero(), parColumn);
						//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = parColumn};
					}
				} else if (c != '+'){
					ungetc(c, stdin);
					parColumn = *column;
					(*column)--;
					running = false;
				}
			}

			if(index > 0){
				Poly output = PolyAddMonos(index, monos);
				free(monos);
				return NewLoadPolyResult(true, output, parColumn);
				//return (LoadPolyResult) {.success = true, .p = output, .column = parColumn};
			} else { 
				return NewLoadPolyResult(false, PolyZero(), parColumn);
				//return (LoadPolyResult) { .success = false, .p = PolyZero(), .column = parColumn};
			}	
		} else {
			ungetc(c, stdin);
			parColumn = *column;
			(*column)--;
			running = false;
			return NewLoadPolyResult(false, PolyZero(), parColumn);
			//return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = parColumn};
		}
	}
	return (LoadPolyResult){ .success = false, .p = PolyZero(), .column = parColumn};
}

LoadCommandResult LoadCommand(int* row){
	char* command = malloc(sizeof(char) * COMMAND_MAX_LENGTH);
	bool commandSuccess = LoadWord(command);

	LoadCommandResult output;
	if(commandSuccess) {
		int commandId = ParseCommand(command);
		if(commandId != ID_COMMAND_NOT_FOUND) {
			int column = 0;
			parser_arithmetic_t param = 0;
			bool success = true;
			bool empty = true;

			if(commandId == ID_AT) {
				char space = getchar();
				if(space == ' ') {
					success = LoadNumber(LONG_MIN, LONG_MAX, &param, &column) == -1;
				} else {
					ungetc(space, stdin);
					success = false;
				}
			} else if(commandId == ID_DEG_BY || commandId == ID_COMPOSE) {
				char space = getchar();
				if(space == ' ') {
					success = LoadNumber(0, UINT_MAX, &param, &column) == -1;
				} else {
					ungetc(space, stdin);
					success = false;
				}
			}

			empty = EatLine();

			if(success && empty) {
				output = NewLoadCommandResult(true, ERROR_NOT_AN_ERROR, commandId, param);
			} else {
				if(commandId == ID_AT){
					output = NewLoadCommandResult(false, ERROR_VALUE, 0, 0);
				} else if (commandId == ID_DEG_BY) {
					output = NewLoadCommandResult(false, ERROR_VARIABLE, 0, 0);
				} else if(commandId == ID_COMPOSE) {
					output = NewLoadCommandResult(false, ERROR_COUNT, 0, 0);
				} else {
					output = NewLoadCommandResult(false, ERROR_COMMAND, 0, 0);
				}	
			}
		} else {
			output = NewLoadCommandResult(false, ERROR_COMMAND, 0, 0);
			EatLine();
		}
	} else {
		output = NewLoadCommandResult(false, ERROR_COMMAND, 0, 0);
		EatLine();
	}

	(*row)++;
	free(command);
	return output;
}

void PrintError(int errorId, int row, int column){
	switch(errorId){
		case ERROR_COMMAND:
			fprintf(stderr, "ERROR %d WRONG COMMAND\n", row);
			break;
		case ERROR_VARIABLE:
			fprintf(stderr, "ERROR %d WRONG VARIABLE\n", row);
			break;
		case ERROR_VALUE:
			fprintf(stderr, "ERROR %d WRONG VALUE\n", row);
			break;
		case ERROR_POLY:
			fprintf(stderr, "ERROR %d %d\n", row, column);
			break;
		case ERROR_UNDERFLOW:
			fprintf(stderr, "ERROR %d STACK UNDERFLOW\n", row);		
			break;
		case ERROR_COUNT:
			fprintf(stderr, "ERROR %d WRONG COUNT\n", row);
			break;
		default:
			break;
	}
}

static inline bool IsDigit(char c)
{
	return (c >= '0' && c <= '9');
}

static parser_arithmetic_t FastPow(parser_arithmetic_t base, int n)
{
	if (n == 0) {
		return 1;
	}

	if (n % 2 == 1) {
		parser_arithmetic_t r = FastPow(base, (n-1)/2);
		return base * r * r;
	}

	parser_arithmetic_t r = FastPow(base, n/2);
	return r*r;
}

static bool LoadWord(char* command)
{
	for(int i = 0; i < COMMAND_MAX_LENGTH; i++) {
		command[i] = '\0';
	}

	char c;
	int index = 0;
	bool running = true;
	bool success = true;
	while(running && (c = getchar())) {
		if((IsLetter(c) || c == '_') && index < COMMAND_MAX_LENGTH-1) {
			command[index++] = c;
		} else if(c == ' ' || c == '\n' || c == '\0' || c == EOF) {
			ungetc(c, stdin);
			running = false;
			success = true;
		} else {
			ungetc(c, stdin);
			running = success = false;
		}
		
	}
	command[index] = '\0'; 

	return success;
}

static bool IsInputBiggerThanBound(char* input, int input_length, parser_arithmetic_t number)
{
	int coeff = number < 0 ? -1 : 1;

	for(int i = 0; i < input_length; i++) {
		int inputDigit = (int)(input[i]-'0');
		int numberDigit = ((number / FastPow(10,(input_length-1-i)))%10)*coeff;
		if( inputDigit > numberDigit ) {
			return true;
		} else if (inputDigit < numberDigit ) {
			return false;
		}
	}
	return false;
}

static int LoadNumber(parser_arithmetic_t low_bound, parser_arithmetic_t high_bound, parser_arithmetic_t* output, int* column){
	assert(low_bound <= 0);
	assert(high_bound >= 0);

	int low_bound_length = 0;
	int high_bound_length = 0;
	parser_arithmetic_t low_bound_tmp = low_bound; 
	parser_arithmetic_t high_bound_tmp = high_bound; 

	do {
		low_bound_tmp /= 10;
		low_bound_length++;
	} while(low_bound_tmp <= -1);

	do {
		high_bound_tmp /= 10;
		high_bound_length++;
	} while(high_bound_tmp >= 1);

	int length = high_bound_length > low_bound_length ? high_bound_length : low_bound_length;

	char* number = malloc((length+1)*sizeof(char));
	int index = 0;
	bool running = true;
	bool is_negative = false;
	char c;
	bool success = true;
	int err_c = 0;
	while(running && (c = getchar())) {
		err_c++;
		(*column)++;

		if(c == '-' && index == 0) {
			is_negative = true;
		} else if (IsDigit(c) && (index <= high_bound_length || is_negative) && (index <= low_bound_length || !is_negative)) {
			number[index++] = c;

			if(is_negative) {
				if(low_bound_length < index) {
					running = success = false;
				} else if(low_bound_length == index) {
					if(IsInputBiggerThanBound(number, index, low_bound)) {
						running = success = false;
					}
				}
			} else {
				if(high_bound_length < index){
					running = success = false;
				} else if(high_bound_length == index) {
					if(IsInputBiggerThanBound(number, index, high_bound)) {
						running = success = false;
					}
				}
			}
		} else if (IsDigit(c) && ((index > high_bound_length && !is_negative) || (index > low_bound_length && is_negative))) {
			ungetc(c, stdin);
			(*column)--;
			success = false;
			running = false;
		} else {
			ungetc(c, stdin);
			(*column)--;
			success = (index > 0);
			err_c = (index > 0) ? -1 : 1;
			running = false;
		}	
	}

	if(success){
		*output = 0;
		for(int i = 0; i < index; i++) {
			*output += ((parser_arithmetic_t)(number[index-1-i]-'0'))*FastPow(10,i)*(is_negative ? -1 : 1);
		}
		
	}

	free(number);
	return success? -1 : err_c;
}

static void LoadPolyCleanup(Mono* monos, int index){
	for(int i = 0; i < index; i++) {
		MonoDestroy(&(monos[i]));
	}
	free(monos);
}

static LoadCommandResult NewLoadCommandResult(bool s, int e, int c, parser_arithmetic_t p)
{
	return (LoadCommandResult) { .success = s, .errorId = e, .commandId = c, .param = p};
}

static LoadPolyResult NewLoadPolyResult(bool s, Poly p, int c)
{
	return (LoadPolyResult) { .success = s, .column = c, .p = p};
}
