/** @file
   Kalkulator dla operacji na wielomianach. 
   Wczytuje wielomiany i komendy z wejścia używając parsera.
   Wielomiany trzymane są na stosie(PolyStack).
   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @date 2017-05-22
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include "utils.h"
#include "poly.h"
#include "stack.h"
#include "parser.h"

/**
 * Wykonuje komendę dla stosu wielomianów.
 * @param[in] s : Stos wielomianów.
 * @param[in] command : Id komendy która ma zostać wykonana.
 * @param[in] param : parametr dla komendy, jeżeli jest jej potrzebny. Jeśli komenda nie
 wymaga parametru, to może być dowolny.
 * @param[in] row : numer wiersza w którym znajduje się komenda
 * @return Bool, true jeżeli udało się wykonać komendę, false w przeciwnym przypadku.
 */
bool ExecuteCommand(PolyStack* s, int command, int param, int row){
	bool success = true;

	if(command == ID_ZERO) {
		Poly p = PolyZero();
		StackPush(s, p);
	} else if(command == ID_IS_COEFF) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			printf("%d\n",PolyIsCoeff(&p));
		} else {
			success = false;
		}	
	} else if(command == ID_IS_ZERO) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			printf("%d\n",PolyIsZero(&p));
		} else {
			success = false;
		}
	} else if(command == ID_CLONE) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			Poly p2 = PolyClone(&p);
			StackPush(s, p2);
		} else {
			success = false;
		}
	} else if(command == ID_ADD) {
		if(StackVerifySize(s, 2)) {
			Poly p = StackPop(s);
			Poly p2 = StackPop(s);
			Poly p_res = PolyAdd(&p, &p2);
			StackPush(s, p_res);
			PolyDestroy(&p);
			PolyDestroy(&p2);
		} else {
			success = false;
		}
	} else if(command == ID_MUL) {
		if(StackVerifySize(s, 2)) {
			Poly p = StackPop(s);
			Poly p2 = StackPop(s);
			Poly p_res = PolyMul(&p, &p2);
			StackPush(s, p_res);
			PolyDestroy(&p);
			PolyDestroy(&p2);
		} else {
			success = false;
		}
	} else if(command == ID_NEG) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPop(s);
			Poly p_res = PolyNeg(&p);
			StackPush(s, p_res);
			PolyDestroy(&p);
		} else {
			success = false;
		}
	} else if(command == ID_SUB) {
		if(StackVerifySize(s, 2)) {
			Poly p = StackPop(s);
			Poly p2 = StackPop(s);
			Poly p_res = PolySub(&p, &p2);
			StackPush(s, p_res);
			PolyDestroy(&p);
			PolyDestroy(&p2);
		} else {
			success = false;
		}
	} else if(command == ID_IS_EQ) {
		if(StackVerifySize(s, 2)) {
			Poly p = StackPop(s);
			Poly p2 = StackPeek(s);
			printf("%d\n",PolyIsEq(&p, &p2));
			StackPush(s, p);
		} else {
			success = false;
		}
	} else if(command == ID_DEG) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			printf("%d\n",PolyDeg(&p));
		} else {
			success = false;
		}
	} else if(command == ID_DEG_BY) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			printf("%d\n",PolyDegBy(&p, (unsigned)param));
		} else {
			success = false;
		}
	} else if(command == ID_AT) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPop(s);
			Poly p_res = PolyAt(&p, (poly_coeff_t)param);
			StackPush(s, p_res);
			PolyDestroy(&p);
		} else {
			success = false;
		}
	} else if(command == ID_PRINT) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPeek(s);
			PolyPrint(&p);
		} else {
			success = false;
		}	
	} else if(command == ID_POP) {
		if(StackVerifySize(s, 1)) {
			Poly p = StackPop(s);
			PolyDestroy(&p);
		} else {
			success = false;
		}	
	} else if(command == ID_PUSHONE) {
		Poly p = PolyFromCoeff(1);
		StackPush(s, p);
	} else if(command == ID_COMPOSE) {
		if((unsigned)param != UINT_MAX && StackVerifySize(s, (unsigned)param+1)) {
			Poly poly_base = StackPop(s);
			Poly* poly_table = malloc(sizeof(Poly)*param);
			for(int i = 0; i < param; i++) {
				Poly p = StackPop(s);
				poly_table[i] = p;
			}

			Poly compose = PolyCompose(&poly_base, param, poly_table);
			StackPush(s, compose);

			PolyDestroy(&poly_base);
			for(int i = 0; i < param; i++) {
				PolyDestroy(&(poly_table[i]));
			}
			free(poly_table);
		} else {
			success = false;
		}
	} else {
		success = false;
	}

	if(!success){
		PrintError(ERROR_UNDERFLOW, row, 0);
		//fprintf(stderr, "ERROR %d STACK UNDERFLOW\n", row);
	}

	return success;
}

/**
 * main
 * @param[in] argc : argc
 * @param[in] argv : argv
 * @returns int
 */
int main(int argc, char **argv)
{
	(void)(argc);
	(void)(argv);

	PolyStack s = StackNew();
	PolyStack* sptr = &s;

	bool running = true;
	int row = 1;
	while(running) {
		char c = getchar();
		ungetc(c, stdin);

		if(c != EOF) {
			if(IsLetter(c)) {
				LoadCommandResult load = LoadCommand(&row);
				if(load.success){
					ExecuteCommand(sptr, load.commandId, load.param, row-1);
				} else {
					PrintError(load.errorId, row-1, 0);
				}
			} else {
				int column = 0;
				LoadPolyResult p = LoadPoly(row, &column, 0);
				bool empty = EatLine();

				row++;
				if(p.success) {
					if(empty){
						StackPush(sptr, p.p);
					} else {
						PolyDestroy(&(p.p));
						PrintError(4, row-1, p.column);
					}
				} else {
					PrintError(4, row-1, p.column);
				}
			}
		} else {
			running = false;
		}
		
	}
	StackDestroy(sptr);

	return 0;
}
